## TouchSides C# Josh Goodman##

The application performs exceptionally well processing the book War and Peace by Leo Tolstoy in less than 3 seconds.

Responsiveness was attempted to be achieved through feedback of the application, telling the user the current status.

The user is always able to perform the next action intuitively.

We can improve the application in multiple ways, here are some ideas:

* Feed back in the form of percentage of file processing complete.
* Giving the user more results and valuable data.