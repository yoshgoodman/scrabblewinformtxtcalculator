﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ScrabbleWinForms.Properties;

namespace ScrabbleWinForms
{
    public partial class ScrabbleTxtFileReader : Form
    {
        private OpenFileDialog _dialog;
        private Hashtable _wordsCount = new Hashtable();

        public ScrabbleTxtFileReader()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _dialog = new OpenFileDialog
            {
                Filter = Resources.FileDialogTxtFilter,
                InitialDirectory = @"C:\",
                Title = Resources.FileDialogTxtTitle
            };

            if (_dialog.ShowDialog() != DialogResult.OK) return;

            _wordsCount = new Hashtable();
            var maxKey = "";
            var maxValue = 0;
            var maxCharKey = "";
            var maxCharValue = 0;
            var highestScrabbleWord = "";
            var charCounter = (int)numCharCounter.Value;

            if (!_dialog.FileName.EndsWith(".txt"))
            {
                lblMostCommonWord.Text = Resources.txtFileNotFound;
                return;
            }

            using (var fileStream = _dialog.OpenFile())
            using (var reader = new StreamReader(fileStream))
            {
                do
                {
                    var line = reader.ReadLine();
                    if (line == null)
                    {
                        break;
                    }

                    var words = Regex.Split(line, @"\W+");

                    foreach (var cleanWord in words.Select(word => word.ToLower().Trim()).Where(cleanWord => !cleanWord.Equals("")))
                    {
                        WordCountUpdater(cleanWord);
                        maxCharValue = CommonWordUpdater(cleanWord, charCounter, maxCharValue, ref maxCharKey);

                        if (highestScrabbleWord.Equals("") ||
                            ScrabbleCalculator.GetPoints(highestScrabbleWord) < ScrabbleCalculator.GetPoints(cleanWord))
                        {
                            highestScrabbleWord = cleanWord;
                        }

                        maxValue = MaxValueUpdater(cleanWord, maxValue, ref maxKey);
                    }
                } while (true);
            }

            OutputBuilder(maxKey, maxValue, charCounter, maxCharKey, maxCharValue, highestScrabbleWord,
                ScrabbleCalculator.GetPoints(highestScrabbleWord));
        }

        private int MaxValueUpdater(string cleanWord, int maxValue, ref string maxKey)
        {
            if ((int)_wordsCount[cleanWord] <= maxValue) return maxValue;
            maxValue = (int)_wordsCount[cleanWord];
            maxKey = cleanWord;
            return maxValue;
        }

        private int CommonWordUpdater(string cleanWord, int charCounter, int maxCharValue, ref string maxCharKey)
        {
            if (!cleanWord.Count().Equals(charCounter)) return maxCharValue;
            if (maxCharValue >= (int)_wordsCount[cleanWord]) return maxCharValue;
            maxCharValue = (int)_wordsCount[cleanWord];
            maxCharKey = cleanWord;
            return maxCharValue;
        }

        private void WordCountUpdater(string cleanWord)
        {
            if (_wordsCount.ContainsKey(cleanWord))
            {
                var count = (int)_wordsCount[cleanWord] + 1;
                _wordsCount[cleanWord] = count;
            }
            else
                _wordsCount.Add(cleanWord, 1);
        }

        private void OutputBuilder(string maxKey, int maxValue, int charCounter, string maxCharKey, int maxCharValue,
            string highestScrabbleWord, int highestScrabbleWordPoints)
        {
            var sb = new StringBuilder();
            lblMostCommonWord.Text = !maxKey.Equals("")
                ? sb.AppendFormat("The most common word is: '{0}' \nwith {1} occurances", maxKey, maxValue).ToString()
                : "The most common word is not found";
            sb.Clear();

            lblCharSelection.Text = !maxCharKey.Equals("")
                ? sb.AppendFormat("The most common word of {0} chars \nis '{1}' with {2} occurances", charCounter,
                    maxCharKey,
                    maxCharValue).ToString()
                : "No words found of selected char size";
            sb.Clear();

            lblScrabbleResult.Text = !highestScrabbleWord.Equals("")
                ? sb.AppendFormat("The highest scoring scrabble word is \n'{0}' with {1} points", highestScrabbleWord,
                    highestScrabbleWordPoints
                    ).ToString()
                : "Highest scrabble word not found";
        }
    }
}