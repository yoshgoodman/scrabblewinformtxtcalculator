﻿namespace ScrabbleWinForms
{
    partial class ScrabbleTxtFileReader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnFileOpen = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblScrabbleResult = new System.Windows.Forms.Label();
            this.lblCharSelection = new System.Windows.Forms.Label();
            this.lblMostCommonWord = new System.Windows.Forms.Label();
            this.numCharCounter = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCharCounter)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnFileOpen
            // 
            this.btnFileOpen.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnFileOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileOpen.Location = new System.Drawing.Point(12, 12);
            this.btnFileOpen.Name = "btnFileOpen";
            this.btnFileOpen.Size = new System.Drawing.Size(260, 23);
            this.btnFileOpen.TabIndex = 0;
            this.btnFileOpen.Text = "Select .txt file";
            this.btnFileOpen.UseVisualStyleBackColor = true;
            this.btnFileOpen.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblScrabbleResult);
            this.groupBox1.Controls.Add(this.lblCharSelection);
            this.groupBox1.Controls.Add(this.lblMostCommonWord);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 76);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 174);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = ".txt Stats";
            // 
            // lblScrabbleResult
            // 
            this.lblScrabbleResult.AutoSize = true;
            this.lblScrabbleResult.Location = new System.Drawing.Point(6, 145);
            this.lblScrabbleResult.Name = "lblScrabbleResult";
            this.lblScrabbleResult.Size = new System.Drawing.Size(0, 13);
            this.lblScrabbleResult.TabIndex = 2;
            // 
            // lblCharSelection
            // 
            this.lblCharSelection.AutoSize = true;
            this.lblCharSelection.Location = new System.Drawing.Point(6, 85);
            this.lblCharSelection.Name = "lblCharSelection";
            this.lblCharSelection.Size = new System.Drawing.Size(0, 13);
            this.lblCharSelection.TabIndex = 1;
            // 
            // lblMostCommonWord
            // 
            this.lblMostCommonWord.AutoSize = true;
            this.lblMostCommonWord.Location = new System.Drawing.Point(6, 25);
            this.lblMostCommonWord.Name = "lblMostCommonWord";
            this.lblMostCommonWord.Size = new System.Drawing.Size(0, 13);
            this.lblMostCommonWord.TabIndex = 0;
            // 
            // numCharCounter
            // 
            this.numCharCounter.Location = new System.Drawing.Point(218, 41);
            this.numCharCounter.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numCharCounter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCharCounter.Name = "numCharCounter";
            this.numCharCounter.Size = new System.Drawing.Size(54, 20);
            this.numCharCounter.TabIndex = 2;
            this.numCharCounter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numCharCounter.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // ScrabbleTxtFileReader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.numCharCounter);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnFileOpen);
            this.MaximumSize = new System.Drawing.Size(300, 300);
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "ScrabbleTxtFileReader";
            this.Text = "Scrabble .txt File Reader";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCharCounter)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnFileOpen;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblScrabbleResult;
        private System.Windows.Forms.Label lblCharSelection;
        private System.Windows.Forms.Label lblMostCommonWord;
        private System.Windows.Forms.NumericUpDown numCharCounter;
    }
}

